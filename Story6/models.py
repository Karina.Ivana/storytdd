from django.db import models

class StatusHarian (models.Model):
    curhat = models.TextField (max_length = 300) 
    waktu = models.DateTimeField(auto_now_add = True)

class RegistrasiMember(models.Model):
    name = models.CharField(max_length = 40)
    email = models.EmailField(max_length = 100, unique = True)
    password = models.CharField(max_length = 40)