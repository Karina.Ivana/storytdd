from django import forms

class Form_Curhat(forms.Form):

    curhat = {
        'type' : 'text',
        'class' :'todo-form-textarea',
        'placeholder' : 'Masukan status...',
        'cols': 50,
        'rows': 4,
        'id' :'id_curhat'
    }

    curhat = forms.CharField(label= 'Apa aja yang terjadi hari ini?', max_length = 320, widget = forms.TextInput(attrs = curhat))

class Form_Regis(forms.Form):

    name = {
        'type' : 'text',
        'class' :'todo-form-textarea',
        'placeholder' : 'Name',
        'cols': 50,
        'rows': 4,
        'id' :'id_name'
    }

    email = {
        'type' : 'text',
        'class' :'todo-form-textarea',
        'placeholder' : 'E-mail',
        'cols': 50,
        'rows': 4,
        'id' :'id_email'
    }

    password = {
        'type' : 'password',
        'class' :'todo-form-textarea',
        'placeholder' : 'Password',
        'cols': 50,
        'rows': 4,
        'id' :'id_password'
    }

    name = forms.CharField(max_length = 40, label = "Name ", widget = forms.TextInput(attrs = name))
    email = forms.EmailField(max_length = 100, label = "E-mail ", widget= forms.EmailInput(attrs = email))
    password = forms.CharField(max_length = 40, min_length = 8, label = "Pasword ", widget=forms.PasswordInput(password))

class Login(forms.Form):
    name = {
        'type' : 'text',
        'class' :'todo-form-textarea',
        'placeholder' : 'Name',
        'cols': 50,
        'rows': 4,
        'id' :'id_name'
    }

    email = {
        'type' : 'text',
        'class' :'todo-form-textarea',
        'placeholder' : 'E-mail',
        'cols': 50,
        'rows': 4,
        'id' :'id_email'
    }
    name = forms.CharField(max_length = 40, label = "Name ", widget = forms.TextInput(attrs = name))
    email = forms.EmailField(max_length = 100, label = "E-mail ", widget= forms.EmailInput(attrs = email))