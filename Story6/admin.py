from django.contrib import admin
from .models import StatusHarian, RegistrasiMember

# Register your models here.
admin.site.register(StatusHarian)
admin.site.register(RegistrasiMember)