from django.urls import path, include
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views
from .views import get_books
from . import views


#url for app
urlpatterns = [
    url("status/", views.Story6, name = "Status" ),
    url("diary/", views.diary, name="diaryku"),
    url("profile/", views.profile, name="ProfileKu"),
    url("createbooks/", views.books, name = "Bukuku"),
    path("newbooks/<str:param>", get_books),
    url("books/", views.Story9, name="Books"),
    url("registration/", views.register, name="Register Akun"),
    url("saveaccount/", views.saveMember, name="Save Account Member"),
    url("validationEmail/", views.validationEmail, name="Validation Email"),
    url("data_subcribe", views.list_data_Member, name = "dataSubcribe"),
    url("result", views.result_Member, name="ListofMember"),
    url("delete/", views.delete_data_Member, name="DeteleMember"),
    url("auth/", include('social_django.urls', namespace='social')), 
    url("login", views.page_Login, name="Login"),
    url("logout", views.logout_view, name = "logout"),
    url("", views.home, name="HomePage")
    
]