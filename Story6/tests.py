from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import Story6, diary, profile, home, Story9
from .models import StatusHarian
from .forms import Form_Curhat
from django.utils import timezone


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class Story6_Test(TestCase):
    def test_url_status(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_func_status(self):
        found = resolve('/status/')
        self.assertEqual(found.func, Story6)

    def test_template_status(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'Story6.html')

    def test_url_profile(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_func_profile(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_template_profile(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_url_home(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_url_books(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_template_books(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'Story9.html')

    def test_url_subcribe(self):
        response = Client().get('/result')
        self.assertEqual(response.status_code, 200)
    
    def test_template_subcribe(self):
        response = Client().get('/result')
        self.assertTemplateUsed(response, 'tableMember.html')

    def test_url_regis(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_regis(self):
        response = Client().get('/registration/')
        self.assertTemplateUsed(response, 'register.html')

    def test_url_createbooks(self):
        response = Client().get('/createbooks/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_newbooks(self):
        response = Client().get('/newbooks/')
        self.assertEqual(response.status_code, 200)

    def test_url_list_subcribe(self):
        response = Client().get('/data_subcribe')
        self.assertEqual(response.status_code, 200)

    def test_new_status(self):
        new_activity = StatusHarian.objects.create(curhat = "Tulis Status HarianMu", waktu = timezone.now())

        counting_all_available_activity = StatusHarian.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Form_Curhat()
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="id_curhat"', form.as_p())


    def test_form_validation_for_blank_items(self):
        form = Form_Curhat(data={'curhat': '', 'waktu' : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['curhat'],
            ["This field is required."]
        )

    def test_save_POST_request(self):
        test = 'Anonymous'
        response = Client().post('/status/diary/', {'curhat' : test, 'waktu' : '2017-10-12T14:14'})
        self.assertEqual(response.status_code, 200)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertIn("", html_response)

    def test_lab5_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response = Client().post('/status/diary/', {'curhat': '', 'waktu': ''})
        self.assertEqual(response.status_code, 200)

        response= Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_using_method_post_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['curhat'] = "coba-coba"
        diary(request)

    def test_using_method_post_not_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['curhat'] = ""
        diary(request)

    def test_using_method_post_regis_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['name'] = "Karina Ivana"
        request.POST['email'] = "karina@gmail"
        request.POST['password'] = "coba-coba123"
        diary(request)

    def test_using_method_post_regis_not_valid(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['name'] = ""
        request.POST['email'] = ""
        request.POST['password'] = ""
        diary(request)

    def test_url_delete(self):
        response = Client().post('/delete/', data={
            "email": "emailku@gmail.com"
        })
        self.assertEqual(response.status_code, 200)

    def test_url_saveMember(self):
        response = Client().post('/saveaccount/', data={
            "name": "Nama saya",
            "email": "emailku@gmail.com",
            "password": "senanghati"
        })
        self.assertEqual(response.status_code, 200)



class CekWebStatusTest (unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CekWebStatusTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(CekWebStatusTest, self).tearDown()

    def test_can_start_the_web(self):
        browser = self.selenium
        browser.get('http://statustdd.herokuapp.com/status/')
        time.sleep(1)
        self.assertIn('StatusKu', browser.title)
    
    def test_navbar_brand(self):
        browser = self.selenium
        browser.get("http://statustdd.herokuapp.com/profile/")
        inputBrand = browser.find_element_by_class_name('navbar-brand').text
        self.assertIn('Karina Ivana', inputBrand)

    def test_text_quotes(self):
        browser = self.selenium
        browser.get("http://statustdd.herokuapp.com/profile/")
        inputQuotes = browser.find_element_by_id('quotes').text
        self.assertIn('Jangan pernah menyerah', inputQuotes)

    def test_background_html_profile(self):
        browser = self.selenium
        browser.get("http://statustdd.herokuapp.com/profile/")
        color_bg = browser.find_element_by_class_name('textPage2').value_of_css_property('background-color')
        self.assertEqual('rgba(206, 233, 240, 0.192)', color_bg)
        
    def test_color_profile(self):
        browser = self.selenium
        browser.get("http://statustdd.herokuapp.com/profile/")
        color_text = browser.find_element_by_class_name('text').value_of_css_property('color')
        self.assertEqual('rgba(45, 35, 61, 0.91)', color_text)
