from django.shortcuts import render, redirect
from .models import StatusHarian, RegistrasiMember
from .forms import Form_Curhat, Form_Regis
from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
import pytz
import json
import requests

diary_dict = {}
data_member_valid = {}
data_login = {}

def Story6(request):
    diary_dict['author'] = "Karina Ivana"
    diary_dict['data_status'] = StatusHarian.objects.all()
    diary_dict['status_form'] = Form_Curhat
    return render(request, 'Story6.html', diary_dict)

def profile(request):
    response = {}
    return render(request, 'profile.html', response)

def home(request):
    response = {}
    return render(request, "home.html", response)

def Story9(request):
    response = {}
    return render(request, "Story9.html", response)

def result_Member(request):
    return render(request, 'tableMember.html')

def page_Login(request):
    return render(request, "login.html", data_login)

def logout_view(request):
    logout(request)
    return redirect("/")


def diary(request):
    
    statusKu = Form_Curhat(request.POST or None)
    
    if (request.method == 'POST' and statusKu.is_valid()):
        diary_dict['curhat'] = request.POST['curhat']
        daftarCurhat = StatusHarian(curhat = diary_dict['curhat'])
        daftarCurhat.save()
        diary_dict['data_status'] = StatusHarian.objects.all()
        return HttpResponseRedirect('/status/')
    else :
        return HttpResponseRedirect('/status/')

def books(request):
    json_src = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    json_file = json_src.json()
    list_books = {
        'items' : []
    }

    for data in json_file['items']:
        books_data = {}
        books_data['title'] = data['volumeInfo']['title']
        # books_data['author'] = []
        # for books_author in data['volumeInfo']['authors']:
        #     books_data['author'].append(books_author)
        books_data['publishedDate'] = data['volumeInfo']['publishedDate']
        list_books['items'].append(books_data)

    return JsonResponse(list_books)
    
def get_books(request, param):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + param
    json_books = requests.get(url).json()
    list_books_new = {
        'items' : []
    }

    for data_new in json_books['items']:
        books_data_new = {}
        books_data_new['title'] = data_new['volumeInfo']['title']
        list_books_new['items'].append(books_data_new)

    return JsonResponse(list_books_new)
    
def register(request):
    data_member_valid['data_Member'] = RegistrasiMember.objects.all()
    data_member_valid['form_Regis'] = Form_Regis
    return render(request, "register.html", data_member_valid)

def saveMember(request):
    akun_member = Form_Regis(request.POST or None)
    
    if (request.method == 'POST' and akun_member.is_valid()):
        data_member_valid['name'] = request.POST['name']
        data_member_valid['email'] = request.POST['email']
        data_member_valid['password'] = request.POST['password']
        data_member = RegistrasiMember(name = data_member_valid['name'], email = data_member_valid['email'], password = data_member_valid['password'])
        data_member.save()
        data_member_valid['data_Member'] = RegistrasiMember.objects.all()
        return JsonResponse({'is_success': True})

def validationEmail(request):
    if request.method == 'POST':
            email = request.POST['email']
            check_email = RegistrasiMember.objects.filter(email=email)
            if check_email.exists():
                return JsonResponse({'is_exists': True})
            return JsonResponse({'is_exists': False})

def list_data_Member(request):
    dataMember = RegistrasiMember.objects.all().values()
    listDataMember = list(dataMember)
    return JsonResponse({'listDataMember': listDataMember})

def delete_data_Member(request):
    if request.method == 'POST':
        email = request.POST['email']
        RegistrasiMember.objects.filter(email = email).delete()
        member_sekarang = list(RegistrasiMember.objects.all().values())
        return JsonResponse({'deleted' : True, 'now' : member_sekarang})

# def set_user_login(request):
#     if request.user.is_autheticated():
#         request.session['username'] = user.username()
#         return HttpResponseRedirect('/books/')

#     else:
#         return HttpResponseRedirect('/books/')






