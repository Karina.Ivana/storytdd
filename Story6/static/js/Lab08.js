
var myVar;

$(document).ready(function(){

    $("#Theme").click(function(){
        console.log("halo1");
        $("body").toggleClass("layar1");
    });

    $('#json').click(
        function(){
            $.ajax({
                url : "createbooks/",
                type:"get",
                success: function(result){
                    console.log(result);
                    var toptable = "<thead class = 'thead-dark'> <tr> <th scope='col'>Judul</th> \
                                    <th scope='col'>Tanggal Penerbitan </th> \
                                    <th scope='col'>Favorite</th> </tr> </thead>";

                    $("#tableBooks").append(toptable);
                    var data = result.items;
                    var tmp;
                    for(i=0; i<data.length; i++){
                        var title = "<tr><td>" + data[i].title + "</td>";
                        //var authorloop = data[i].author;
                        // var author = "<td>";
                        // for(x=0; x<authorloop.length; x++){
                        //     var author =  author + authorloop[x] + "<br>";
                        // }
                        // var authorakhir = author + "</td>";
                        var date = "<td>" + data[i].publishedDate + "</td>"; 
                        var button = "<img id=" + i + ' onclick="changeImage(id)" width="25" height="25" src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';
                        var buttonfav = "<td>" + button + "</td></tr>";
                        var tmp = title + date + buttonfav;
                        $("#tableBooks").append(tmp);
                    }
                }
            });
        });

    
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        var email_is_available;
        var timer = 0;
    
        $('#id_email').keydown(function () {
            clearTimeout(timer);
            timer = setTimeout(checkValidEmailFormat, 1000);
            toggleButton();
        });
    
        $('#id_name').keydown(function () {
            clearTimeout(timer);
            timer = setTimeout(toggleButton, 1000);
        });
    
        $('#id_password').keydown(function () {
            clearTimeout(timer);
            timer = setTimeout(toggleButton, 1000);
        });
    
        $('#form').on('submit', function (event) {
            event.preventDefault();
            console.log("Form Submitted!");
            sendFormData();
        });

        function validateEmail() {
            $.ajax({
                method: 'POST',
                url: "/validationEmail/",
                headers: {
                    "X-CSRFToken": csrftoken,
                },
                data: {
                    email: $('#id_email').val(),
                },
                success: function (email) {
                    if (email.is_exists) {
                        email_is_available = false;
                        $('.definition_text_red p').replaceWith("<p class='fail'>This email is already been used, please use another email!</p>");
                    } else {
                        email_is_available = true;
                        toggleButton();
                    }
                },
                error: function () {
                    alert("Error, cannot validate email!")
                }
            })
        }
        
        function sendFormData() {
            $.ajax({
                method: 'POST',
                url: "/saveaccount/",
                headers: {
                    "X-CSRFToken": csrftoken,
                },
                data: {
                    name: $('#id_name').val(),
                    email: $('#id_email').val(),
                    password: $('#id_password').val(),
                },
                success: function (response) {
                    if (response.is_success) {
                        $('#id_name').val('');
                        $('#id_email').val('');
                        $('#id_password').val('');
                        $('#button_regis').prop('disabled', true);
                        $('.definition_text_red p').replaceWith("<p style='color:green' >Data successfully saved!</p>");
                        console.log("Successfully add data");
                    } else {
                        $('.definition_text_red p').replaceWith("<p>Error! Data cannot be saved!</p>");
                    }
                },
                error: function () {
                    alert("Error, cannot save data to database");
                }
            })
        }
        
        function checkValidEmailFormat() {
            var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var is_valid = reg.test($('#id_email').val());
        
            if (is_valid) {
                validateEmail();
            } else {
                $('.definition_text_red p').replaceWith("<p>Please enter a valid email format!</p>");
            }
        }
        
        function toggleButton() {
            var password = $('#id_password').val();
            var name = $('#id_name').val();
            var email = $('#id_email').val();
            if (password.length !== 0 && name.length !== 0) {
                $('.definition_text_red p').replaceWith("<p></p>");
                $('#button_regis').prop('disabled', false);
            } else if (password.length === 0 && name.length === 0) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Name and password cannot be empty</p>");
            } else if (password.length === 0 && email.length === 0) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Email and password cannot be empty</p>");
            } else if (name.length === 0 && email.length) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Name and email cannot be empty</p>");
            } else if (password.length === 0) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Password cannot be empty</p>");
            } else if (name.length === 0) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Name cannot be empty</p>");
            } else if (email.length === 0) {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Email cannot be empty</p>");
            } else {
                $('#button_regis').prop('disabled', true);
                $('.definition_text_red p').replaceWith("<p>Please enter a valid email format!</p>");
            }
        }

        $.ajax({
            // method: 'POST',
            url: '/data_subcribe/',
            // headers: {
            //     "X-CSRFToken": csrftoken
            // },
            success: function (result) {
                var length_arr = result.listDataMember.length;
                var data = result.listDataMember;
                for (var i = 0; i < length_arr; i++) {
                    var name = data[i]['name'];
                    var email = data[i]['email'];
                    var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                    var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                    $('#tbodySubscribe').append(show)
                }
            }
        });

        
        
        
        
        
});

function unsubscribe(t) {
    var txt;
    var person = prompt("Please enter your password:", "password");
    if (person == null || person == "") {
        txt = "User cancelled the prompt.";
    } else {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        var email = t.id;
        $.ajax({
            url: '/data_subcribe',
            success: function (result) {
                var length_arr = result.listDataMember.length;
                var data = result.listDataMember;
                for (var i = 0; i < length_arr; i++) {
                    if(email ==  data[i]['email']) {
                        if(person == data[i]['password']) {
                            $.ajax({
                                method: 'POST',
                                url: "/delete/",
                                headers: {
                                    "X-CSRFToken": csrftoken
                                },
                                data: {email: email},
                                success: function (res) {
                                    console.log('success');
                                    $('#tbodySubscribe').replaceWith("<tbody id='tbodySubscribe'></tbody>");
                    
                                    var length_arr = res.now.length;
                                    var data = res.now;
                                    for (var i = 0; i < length_arr; i++) {
                                        var name = data[i]['name'];
                                        var email = data[i]['email'];
                                        var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                                        var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                                        $('#tbodySubscribe').append(show)
                                    }
                                }
                            })
                        } else {
                            alert("Password yang anda masukkan salah");
                        }
                    }
                }
            }
        })
    }  
};

function renderResult() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: 'POST',
        url: '/data_subcribe/',
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function (result) {
            var length_arr = result.listDataMember.length;
            var data = result.listDataMember;
            for (var i = 0; i < length_arr; i++) {
                var name = data[i]['name'];
                var email = data[i]['email'];
                var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                var show = '<tr>' + '<td>' + name + '</td><td>' + email + '</td><td>' + button + '</td>';
                $('#tbodySubscribe').append(show)
            }
        }
    })
};

function detail(){
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
    }
}

function loadingFunction() {
    myVar = setTimeout(showPage, 500);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

function deletebutton() {
    document.getElementById("json").style.display = "none"; 
    document.getElementById("data").style.display = "block";
}

function deletebuttonCariBuku() {
    document.getElementById("submitCariBuku").style.display = "none"; 
}


function changeImage(id) {
    $.ajax({
        url: "createbooks/",
        method: "GET",
        success: function(hasil) {
            count = document.getElementById('counter').innerHTML;
            count = parseInt(count);
            console.log(count);
            var data = hasil.items;
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                var id2 = i;
                if(id == id2) {
                    var image = document.getElementById(id2);
                    if (image.src.match("https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png")) {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/favorited.png";
                        count = count + 1;
                    } else {
                        image.src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png";
                        count = count - 1;
                    }
                }
            }
            $('#counter').replaceWith('<span id="counter" style="font-size : 2vw">' + count + '</span>');
        },
        error: function(error) {
            alert("Halaman buku tidak ditemukan")
        }
    });
}

function cariBuku(data) {
    var valueSearch = document.getElementById("searchBooks").value;
    console.log(valueSearch);
    $.ajax({
        method: "GET",
        url: "/newbooks/" + valueSearch,
        success: function(result){
            console.log(result);
            var toptable = "<thead class = 'thead-dark'> <tr> <th scope='col'>Judul</th> \
                            <th scope='col'>Favorite</th> </tr> </thead>";

            $("#tableSearchBooks").append(toptable);
            var data = result.items;
            var tmp;
            for(i=0; i<data.length; i++){
                var title = "<tr><td>" + data[i].title + "</td>";
                var button = "<img id=" + i + ' onclick="changeImage(id)" width="25" height="25" src = "https://gitlab.com/georgematthewl/story9-ppw/raw/master/static/assets/default.png">';
                var buttonfav = "<td>" + button + "</td></tr>";
                var tmp = title +  buttonfav;
                $("#tableSearchBooks").append(tmp);
            }
        }
    });
}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
function init() {
    gapi.load('auth2', function() { // Ready. });
  });
}


function signOut() {
    
}









